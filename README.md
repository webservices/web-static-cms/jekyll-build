This image was created with the purpose to make the publishing process of websites, running with Jekyll, faster.
As a first step with have this custom Docker image with pre-installed gems, running in gitlab-registry.

use: `gitlab-registry.cern.ch/webservices/web-static-cms/jekyll-build:latest` in gitlabci.yml to use the latest image.

Gems not installed by default needed to build the website will be installed in runtime. But this costs times so consider add new and recurrently used, gems to this project's gemfile.

To rebuild this image run locally:
```
docker build -t gitlab-registry.cern.ch/webservices/web-static-cms/jekyll-build .
```
And publish to gitlab-registry
```
docker push gitlab-registry.cern.ch/webservices/web-static-cms/jekyll-build
```

The idea is to make our user's publishing process (user's git commit + build + publish on EOS/DFS) as fast as possible aiming to be under 10 seconds.
Options to explore in the future are optimizing the current job which is deploying files on DFS (build service on Web Infra?) and adding dedicated gitlab runners to execute this image.